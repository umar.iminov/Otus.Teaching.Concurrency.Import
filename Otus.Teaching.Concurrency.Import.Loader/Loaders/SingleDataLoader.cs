﻿using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System;
using System.Diagnostics;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class SingleDataLoader
        : IDataLoader
    {
        private readonly List<Customer> _customerList;
        public SingleDataLoader(List<Customer> customerList)
            => _customerList = customerList;

        public void LoadData()
        {
            Console.WriteLine($"Loading data... single thread mode");
            var sw = new Stopwatch();
            sw.Start();
            var ctx = new CustomerRepository();
            
            ctx.AddCustomerRange(_customerList);
            ctx.SaveChanges();
            sw.Stop();
            Console.WriteLine($"Loaded data. Mode single. Took {sw.Elapsed.TotalSeconds:F}");
        }
    }
}