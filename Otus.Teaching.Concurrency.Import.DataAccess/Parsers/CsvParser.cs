﻿using Microsoft.VisualBasic.FileIO;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        public List<Customer> Parse(string path)
        {
            using (TextFieldParser parser = new TextFieldParser(path))
            {
                List<Customer> customerList = new List<Customer>();
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    if (TryValidate(fields))
                    {
                        var customer = new Customer()
                        {
                            Id = int.Parse(fields[0]),
                            Email = fields[1],
                            Phone = fields[2],
                            FullName = fields[3]
                        };
                        customerList.Add(customer);
                    }
                }
                return customerList;
            }
        }

        private static bool TryValidate(string[] fields)
        {
            if (fields.Length == 4)
                return true;
            else
                return false;
        }
    }
}
