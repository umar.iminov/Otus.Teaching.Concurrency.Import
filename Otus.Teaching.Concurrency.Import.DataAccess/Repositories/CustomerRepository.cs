using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    { private readonly AppDbContext _dbcontext = new();
        
    
        public void AddCustomer(Customer customer)
        {
            //Add customer to data source   
            _dbcontext.Customers.Add(customer);
        }

        public void AddCustomerRange(List<Customer> customers)
        {
            //Add customer to data source   
            _dbcontext.Customers.AddRange(customers);
        }

        public void Reset()
        {
            _dbcontext.Database.ExecuteSqlInterpolated($"truncate table \"Customers\"");
            //_dbcontext.Customers.RemoveRange(_dbcontext.Customers);
            //_dbcontext.SaveChanges();
        }

        public void SaveChanges()
        {
            _dbcontext.SaveChanges();
        }
    }
}