﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.DataAccess.Migrations;
using System.Configuration;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class AppDbContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            string connectionString = "Host = localhost; Port = 5432; Database = customerDB; Username = postgres; Password = Start123";

            optionsBuilder.UseNpgsql(connectionString);
        }
    }
}
