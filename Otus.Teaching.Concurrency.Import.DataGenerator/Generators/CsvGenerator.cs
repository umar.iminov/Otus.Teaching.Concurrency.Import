﻿using Otus.Teaching.Concurrency.Import.Handler.Data;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CsvGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using (StreamWriter writer = new StreamWriter(new FileStream(_fileName, FileMode.Create, FileAccess.Write)))
            {
                foreach (var customer in customers)
                {
                    writer.WriteLine($"{customer.Id},{customer.FullName},{customer.Email},{customer.Phone}");
                }
            }
        }
    }
}